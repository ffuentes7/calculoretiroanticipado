package AIT.microservicios.model.share;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name ="HEADER")
@XmlAccessorType(XmlAccessType.FIELD)
public class Header {

    @XmlElement(name = "CHANNEL")
    private String channel;
    @XmlElement(name = "RMT_SRVR")
    private String rmtSvr;
    @XmlElement(name = "CORRELATIONID")
    private String correlationId;
    @XmlElement(name = "FUNCTION")
    private String function;
    @XmlElement(name = "RESPONSE_QUEUE")
    private String responseQueue;
    @XmlElement(name = "RESPONSE_QMGR")
    private String responseQmgr;
    @XmlElement(name = "START_TIME")
    private String startTime;
    @XmlElement(name = "END_TIME")
    private String endTime;
    @XmlElement(name = "ERRORCODE")
    private String errorCode;
    @XmlElement(name = "ERRORDESCRIPTION")
    private String errorDescription;
    @XmlElement(name = "PAGING")
    private String paging;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getRmtSvr() {
        return rmtSvr;
    }

    public void setRmtSvr(String rmtSvr) {
        this.rmtSvr = rmtSvr;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getResponseQueue() {
        return responseQueue;
    }

    public void setResponseQueue(String responseQueue) {
        this.responseQueue = responseQueue;
    }

    public String getResponseQmgr() {
        return responseQmgr;
    }

    public void setResponseQmgr(String responseQmgr) {
        this.responseQmgr = responseQmgr;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getPaging() {
        return paging;
    }

    public void setPaging(String paging) {
        this.paging = paging;
    }

    public Header() {
    }

    public Header(String channel, String rmtSvr, String correlationId, String function, String responseQueue, String responseQmgr) {
        this.channel = channel;
        this.rmtSvr = rmtSvr;
        this.correlationId = correlationId;
        this.function = function;
        this.responseQueue = responseQueue;
        this.responseQmgr = responseQmgr;
    }

    public Header(String channel, String rmtSvr, String correlationId, String function, String responseQueue, String responseQmgr, String startTime, String endTime, String errorCode, String errorDescription, String paging) {
        this.channel = channel;
        this.rmtSvr = rmtSvr;
        this.correlationId = correlationId;
        this.function = function;
        this.responseQueue = responseQueue;
        this.responseQmgr = responseQmgr;
        this.startTime = startTime;
        this.endTime = endTime;
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
        this.paging = paging;
    }
}
