package AIT.microservicios.model.responseAS400;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@Data
@XmlRootElement(name = "PRODUCTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class Producto {

    @XmlElement(name = "NUMERO")
    private String numero;
    @XmlElement(name = "MONEDA")
    private String moneda;
    @XmlElement(name = "DESMONEDA")
    private String desMoneda;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getDesMoneda() {
        return desMoneda;
    }

    public void setDesMoneda(String desMoneda) {
        this.desMoneda = desMoneda;
    }

    public Producto() {
    }

    public Producto(String numero, String moneda, String desMoneda) {
        this.numero = numero;
        this.moneda = moneda;
        this.desMoneda = desMoneda;
    }
}
