package AIT.microservicios.model.responseAS400;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@Data
@XmlRootElement(name = "INFORMACION")
@XmlAccessorType(XmlAccessType.FIELD)
public class Informacion {

    @XmlElement(name = "PRODUCTO")
    private Producto producto;
    @XmlElement(name = "CLIENTE")
    private Cliente cliente;
    @XmlElement(name = "CALCULOSANCION")
    private CalculoSancion calculosSancion;

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public CalculoSancion getCalculosSancion() {
        return calculosSancion;
    }

    public void setCalculosSancion(CalculoSancion calculosSancion) {
        this.calculosSancion = calculosSancion;
    }

    public Informacion() {
    }

    public Informacion(Producto producto, Cliente cliente, CalculoSancion calculosSancion) {
        this.producto = producto;
        this.cliente = cliente;
        this.calculosSancion = calculosSancion;
    }
}
