package AIT.microservicios.model.responseAS400;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@Data
@XmlRootElement(name = "SECURITY")
@XmlAccessorType(XmlAccessType.FIELD)
public class Security {

    @XmlElement(name = "METHOD")
    private String method;
    @XmlElement(name = "SUM")
    private String sum;
    @XmlElement(name = "SUM2")
    private String sum2;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getSum2() {
        return sum2;
    }

    public void setSum2(String sum2) {
        this.sum2 = sum2;
    }

    public Security() {
    }

    public Security(String method, String sum, String sum2) {
        this.method = method;
        this.sum = sum;
        this.sum2 = sum2;
    }
}
