package AIT.microservicios.model.responseAS400;

import javax.xml.bind.annotation.*;
@lombok.Data
@XmlRootElement(name = "DATA")
@XmlAccessorType(XmlAccessType.FIELD)
public class Data {

    @XmlAttribute(name = "RESULT")
    private String resultado;
    @XmlElement(name = "INFORMACION")
    private Informacion informacion;

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Informacion getInformacion() {
        return informacion;
    }

    public void setInformacion(Informacion informacion) {
        this.informacion = informacion;
    }

    public Data() {
    }

    public Data(String resultado, Informacion informacion) {
        this.resultado = resultado;
        this.informacion = informacion;
    }
}
