package AIT.microservicios.model.responseAS400;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "CLIENTE")
@XmlAccessorType(XmlAccessType.FIELD)
public class Cliente {

    @XmlElement(name = "NOMBREABREVIADO")
    private String nombreAbreviado;

    public String getNombreAbreviado() {
        return nombreAbreviado;
    }

    public void setNombreAbreviado(String nombreAbreviado) {
        this.nombreAbreviado = nombreAbreviado;
    }

    public Cliente() {
    }

    public Cliente(String nombreAbreviado) {
        this.nombreAbreviado = nombreAbreviado;
    }
}
