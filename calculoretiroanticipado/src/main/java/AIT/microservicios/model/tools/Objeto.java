package AIT.microservicios.model.tools;

import lombok.Data;

@Data
public class Objeto {

    private String xml;
    private String correlID;
    private String error;

    public Objeto(String xml, String correlID, String error) {
        this.xml = xml;
        this.correlID = correlID;
        this.error = error;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getCorrelID() {
        return correlID;
    }

    public void setCorrelID(String correlID) {
        this.correlID = correlID;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Objeto() {
    }
}
