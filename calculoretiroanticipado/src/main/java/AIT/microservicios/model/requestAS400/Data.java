package AIT.microservicios.model.requestAS400;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@lombok.Data
@XmlRootElement(name ="DATA")
@XmlAccessorType(XmlAccessType.FIELD)
public class Data {

    @XmlElement(name = "PRODUCTO")
    public Producto producto;

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Data() {
    }

    public Data(Producto producto) {
        this.producto = producto;
    }
}
