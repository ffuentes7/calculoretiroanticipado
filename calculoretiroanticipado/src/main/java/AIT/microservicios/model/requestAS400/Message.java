package AIT.microservicios.model.requestAS400;

import AIT.microservicios.model.share.Cust_Info;
import AIT.microservicios.model.share.Header;
import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "MESSAGE")
@XmlAccessorType(XmlAccessType.FIELD)
public class Message  {

    @XmlAttribute(name = "TYPE")
    private String type;
    @XmlElement(name = "HEADER")
    private Header header;
    @XmlElement(name = "CUST_INFO")
    private Cust_Info custInfo;
    @XmlElement(name = "DATA")
    private AIT.microservicios.model.requestAS400.Data data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Cust_Info getCustInfo() {
        return custInfo;
    }

    public void setCustInfo(Cust_Info custInfo) {
        this.custInfo = custInfo;
    }

    public AIT.microservicios.model.requestAS400.Data getData() {
        return data;
    }

    public void setData(AIT.microservicios.model.requestAS400.Data data) {
        this.data = data;
    }

    public Message() {
    }

    public Message(String type, Header header, Cust_Info custInfo, AIT.microservicios.model.requestAS400.Data data) {
        this.type = type;
        this.header = header;
        this.custInfo = custInfo;
        this.data = data;
    }
}
