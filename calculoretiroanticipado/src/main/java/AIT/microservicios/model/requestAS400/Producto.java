package AIT.microservicios.model.requestAS400;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "PRODUCTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class Producto {

    @XmlElement(name = "NUMERO")
    private String numero;
    @XmlElement(name = "FECHARETIRO")
    private String fechaRetiro;
    @XmlElement(name = "MONTORETIRO")
    private String montoRetiro;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getFechaRetiro() {
        return fechaRetiro;
    }

    public void setFechaRetiro(String fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    public String getMontoRetiro() {
        return montoRetiro;
    }

    public void setMontoRetiro(String montoRetiro) {
        this.montoRetiro = montoRetiro;
    }

    public Producto() {
    }

    public Producto(String numero, String fechaRetiro, String montoRetiro) {
        this.numero = numero;
        this.fechaRetiro = fechaRetiro;
        this.montoRetiro = montoRetiro;
    }
}
