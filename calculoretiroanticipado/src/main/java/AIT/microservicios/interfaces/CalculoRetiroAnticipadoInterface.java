package AIT.microservicios.interfaces;

import AIT.microservicios.pojo.request.RequestCustom;
import AIT.microservicios.pojo.response.ResponseBroker;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;

@WebService
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface CalculoRetiroAnticipadoInterface {

    @WebMethod
    public ResponseBroker calcularRetiroAnticipado(RequestCustom msj) throws IOException;

}
