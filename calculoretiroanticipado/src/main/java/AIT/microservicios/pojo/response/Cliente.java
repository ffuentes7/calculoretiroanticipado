package AIT.microservicios.pojo.response;

import lombok.Data;

@Data
public class Cliente {

    private String nombreAbreviado;

    public Cliente(String nombreAbreviado) {
        this.nombreAbreviado = nombreAbreviado;
    }

    public Cliente() {
    }

    public String getNombreAbreviado() {
        return nombreAbreviado;
    }

    public void setNombreAbreviado(String nombreAbreviado) {
        this.nombreAbreviado = nombreAbreviado;
    }
}
