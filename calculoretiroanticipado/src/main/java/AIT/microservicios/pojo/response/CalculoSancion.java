package AIT.microservicios.pojo.response;

public class CalculoSancion {
    private String fechaEfectiva;
    private String saldo;
    private String interesAdeudado;
    private String interesRetenido;
    private String tasaInteres;
    private String montoSancion;
    private String sancionNoCubiertaIntaCum;
    private String tasaSancion;
    private String diasFaltantesVencimiento;
    private String intUltimaRenov;
    private String tasaInteresRecalculada;
    private String interesConfiscado;
    private String sancionCapital;
    private String sancionRetiro;
    private String impuestoPagar;
    private String netoCliente;

    public CalculoSancion(String fechaEfectiva, String saldo, String interesAdeudado, String interesRetenido, String tasaInteres, String montoSancion, String sancionNoCubiertaIntaCum, String tasaSancion, String diasFaltantesVencimiento, String intUltimaRenov, String tasaInteresRecalculada, String interesConfiscado, String sancionCapital, String sancionRetiro, String impuestoPagar, String netoCliente) {
        this.fechaEfectiva = fechaEfectiva;
        this.saldo = saldo;
        this.interesAdeudado = interesAdeudado;
        this.interesRetenido = interesRetenido;
        this.tasaInteres = tasaInteres;
        this.montoSancion = montoSancion;
        this.sancionNoCubiertaIntaCum = sancionNoCubiertaIntaCum;
        this.tasaSancion = tasaSancion;
        this.diasFaltantesVencimiento = diasFaltantesVencimiento;
        this.intUltimaRenov = intUltimaRenov;
        this.tasaInteresRecalculada = tasaInteresRecalculada;
        this.interesConfiscado = interesConfiscado;
        this.sancionCapital = sancionCapital;
        this.sancionRetiro = sancionRetiro;
        this.impuestoPagar = impuestoPagar;
        this.netoCliente = netoCliente;
    }

    public CalculoSancion() {
    }

    public String getFechaEfectiva() {
        return fechaEfectiva;
    }

    public void setFechaEfectiva(String fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getInteresAdeudado() {
        return interesAdeudado;
    }

    public void setInteresAdeudado(String interesAdeudado) {
        this.interesAdeudado = interesAdeudado;
    }

    public String getInteresRetenido() {
        return interesRetenido;
    }

    public void setInteresRetenido(String interesRetenido) {
        this.interesRetenido = interesRetenido;
    }

    public String getTasaInteres() {
        return tasaInteres;
    }

    public void setTasaInteres(String tasaInteres) {
        this.tasaInteres = tasaInteres;
    }

    public String getMontoSancion() {
        return montoSancion;
    }

    public void setMontoSancion(String montoSancion) {
        this.montoSancion = montoSancion;
    }

    public String getSancionNoCubiertaIntaCum() {
        return sancionNoCubiertaIntaCum;
    }

    public void setSancionNoCubiertaIntaCum(String sancionNoCubiertaIntaCum) {
        this.sancionNoCubiertaIntaCum = sancionNoCubiertaIntaCum;
    }

    public String getTasaSancion() {
        return tasaSancion;
    }

    public void setTasaSancion(String tasaSancion) {
        this.tasaSancion = tasaSancion;
    }

    public String getDiasFaltantesVencimiento() {
        return diasFaltantesVencimiento;
    }

    public void setDiasFaltantesVencimiento(String diasFaltantesVencimiento) {
        this.diasFaltantesVencimiento = diasFaltantesVencimiento;
    }

    public String getIntUltimaRenov() {
        return intUltimaRenov;
    }

    public void setIntUltimaRenov(String intUltimaRenov) {
        this.intUltimaRenov = intUltimaRenov;
    }

    public String getTasaInteresRecalculada() {
        return tasaInteresRecalculada;
    }

    public void setTasaInteresRecalculada(String tasaInteresRecalculada) {
        this.tasaInteresRecalculada = tasaInteresRecalculada;
    }

    public String getInteresConfiscado() {
        return interesConfiscado;
    }

    public void setInteresConfiscado(String interesConfiscado) {
        this.interesConfiscado = interesConfiscado;
    }

    public String getSancionCapital() {
        return sancionCapital;
    }

    public void setSancionCapital(String sancionCapital) {
        this.sancionCapital = sancionCapital;
    }

    public String getSancionRetiro() {
        return sancionRetiro;
    }

    public void setSancionRetiro(String sancionRetiro) {
        this.sancionRetiro = sancionRetiro;
    }

    public String getImpuestoPagar() {
        return impuestoPagar;
    }

    public void setImpuestoPagar(String impuestoPagar) {
        this.impuestoPagar = impuestoPagar;
    }

    public String getNetoCliente() {
        return netoCliente;
    }

    public void setNetoCliente(String netoCliente) {
        this.netoCliente = netoCliente;
    }
}
