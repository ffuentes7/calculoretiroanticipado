package AIT.microservicios.pojo.response;


public class CalculoRetiroAnticipadoResponse {

    private ProductoResponse productoResponse;
    private Cliente cliente;
    private CalculoSancion calculoSancion;

    public CalculoRetiroAnticipadoResponse(ProductoResponse productoResponse, Cliente cliente, CalculoSancion calculoSancion) {
        this.productoResponse = productoResponse;
        this.cliente = cliente;
        this.calculoSancion = calculoSancion;
    }

    public CalculoRetiroAnticipadoResponse() {
    }

    public ProductoResponse getProductoResponse() {
        return productoResponse;
    }

    public void setProductoResponse(ProductoResponse productoResponse) {
        this.productoResponse = productoResponse;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public CalculoSancion getCalculoSancion() {
        return calculoSancion;
    }

    public void setCalculoSancion(CalculoSancion calculoSancion) {
        this.calculoSancion = calculoSancion;
    }
}
