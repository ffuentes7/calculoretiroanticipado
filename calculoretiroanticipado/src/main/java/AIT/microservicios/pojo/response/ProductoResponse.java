package AIT.microservicios.pojo.response;

import lombok.Data;

@Data
public class ProductoResponse {

    private String numero;
    private String moneda;
    private String desMoneda;

    public ProductoResponse(String numero, String moneda, String desMoneda) {
        this.numero = numero;
        this.moneda = moneda;
        this.desMoneda = desMoneda;
    }

    public ProductoResponse() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getDesMoneda() {
        return desMoneda;
    }

    public void setDesMoneda(String desMoneda) {
        this.desMoneda = desMoneda;
    }
}
