package AIT.microservicios.pojo.response;

import AIT.core.base.pojos.microservicios.Header;
import AIT.core.base.pojos.microservicios.Respuesta;

public class ResponseBroker extends AIT.core.base.pojos.microservicios.Response {

    private CalculoRetiroAnticipadoResponse calculoRetiroAnticipadoResponse;

    public CalculoRetiroAnticipadoResponse getCalculoRetiroAnticipadoResponse() {
        return calculoRetiroAnticipadoResponse;
    }

    public void setCalculoRetiroAnticipadoResponse(CalculoRetiroAnticipadoResponse calculoRetiroAnticipadoResponse) {
        this.calculoRetiroAnticipadoResponse = calculoRetiroAnticipadoResponse;
    }

    public ResponseBroker() {
    }

    @Override
    public Header getHeader() {
        return super.getHeader();
    }

    @Override
    public void setHeader(Header header) {
        super.setHeader(header);
    }

    @Override
    public Respuesta getRespuesta() {
        return super.getRespuesta();
    }

    @Override
    public void setRespuesta(Respuesta respuesta) {
        super.setRespuesta(respuesta);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
