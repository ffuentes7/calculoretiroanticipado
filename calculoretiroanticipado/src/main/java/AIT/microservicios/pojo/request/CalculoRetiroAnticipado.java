package AIT.microservicios.pojo.request;

import lombok.Data;

@Data
public class CalculoRetiroAnticipado {

    private Producto producto;

    public CalculoRetiroAnticipado(Producto producto) {
        this.producto = producto;
    }

    public CalculoRetiroAnticipado() {
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
}
