package AIT.microservicios.pojo.request;

import lombok.Data;

@Data
public class Producto {

    private String numero;
    private String fechaRetiro;
    private String montoRetiro;

    public Producto(String numero, String fechaRetiro, String montoRetiro) {
        this.numero = numero;
        this.fechaRetiro = fechaRetiro;
        this.montoRetiro = montoRetiro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getFechaRetiro() {
        return fechaRetiro;
    }

    public void setFechaRetiro(String fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    public String getMontoRetiro() {
        return montoRetiro;
    }

    public void setMontoRetiro(String montoRetiro) {
        this.montoRetiro = montoRetiro;
    }

    public Producto() {
    }
}
