package AIT.microservicios.pojo.request;

import AIT.core.base.pojos.microservicios.Header;
import lombok.Data;

@Data
public class RequestCustom extends AIT.core.base.pojos.microservicios.Request {

    public RequestCustom(CalculoRetiroAnticipado calculoRetiroAnticipado) {
        this.calculoRetiroAnticipado = calculoRetiroAnticipado;
    }

    public RequestCustom() {
    }

    public CalculoRetiroAnticipado getCalculoRetiroAnticipado() {
        return calculoRetiroAnticipado;
    }

    public void setCalculoRetiroAnticipado(CalculoRetiroAnticipado calculoRetiroAnticipado) {
        this.calculoRetiroAnticipado = calculoRetiroAnticipado;
    }

    private CalculoRetiroAnticipado calculoRetiroAnticipado;

    @Override
    public Header getHeader() {
        return super.getHeader();
    }

    @Override
    public void setHeader(Header header) {
        super.setHeader(header);
    }

    @Override
    public String getTipoDocumento() {
        return super.getTipoDocumento();
    }

    @Override
    public void setTipoDocumento(String tipoDocumento) {
        super.setTipoDocumento(tipoDocumento);
    }

    @Override
    public String getNroDocumento() {
        return super.getNroDocumento();
    }

    @Override
    public void setNroDocumento(String nroDocumento) {
        super.setNroDocumento(nroDocumento);
    }

    @Override
    public String getNroSolicitud() {
        return super.getNroSolicitud();
    }

    @Override
    public void setNroSolicitud(String nroSolicitud) {
        super.setNroSolicitud(nroSolicitud);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
