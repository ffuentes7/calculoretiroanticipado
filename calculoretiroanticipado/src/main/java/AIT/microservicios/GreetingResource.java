package AIT.microservicios;

import AIT.microservicios.pojo.request.RequestCustom;
import AIT.microservicios.pojo.response.ResponseBroker;
import AIT.microservicios.services.CalculoRetiroAnticipadoService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/Siebel/MS/CalculoRetiroAnticipado")
public class GreetingResource {

    @Inject
    CalculoRetiroAnticipadoService calculo;
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseBroker CalcularRetiroAnticipado(RequestCustom rq) {

        return calculo.calcularRetiroAnticipado(rq);
    }
}