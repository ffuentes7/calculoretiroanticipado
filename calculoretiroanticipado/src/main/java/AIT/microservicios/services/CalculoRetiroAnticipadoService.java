package AIT.microservicios.services;

import AIT.core.base.logs.LogFile;
import AIT.core.base.pojos.microservicios.ConsumoMq;
import AIT.core.base.services.microservicios.IbmMqService;
import AIT.core.base.utils.BaseUtils;
import AIT.microservicios.interfaces.CalculoRetiroAnticipadoInterface;
import AIT.microservicios.model.tools.Objeto;
import AIT.microservicios.pojo.request.RequestCustom;
import AIT.microservicios.pojo.response.ResponseBroker;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jws.WebService;
import java.util.Optional;

@ApplicationScoped
@WebService(endpointInterface = "AIT.microservicios.interfaces.CalculoRetiroAnticipadoInterface")
public class CalculoRetiroAnticipadoService implements CalculoRetiroAnticipadoInterface {
    @Inject
    ComunicacionesAS400 consultarAS400;
    IbmMqService mqService = new IbmMqService();
    @ConfigProperty(name = "mq.host")
    String host;
    @ConfigProperty(name = "mq.port")
    int port;
    @ConfigProperty(name = "mq.channel")
    String channel;
    @ConfigProperty(name = "mq.qmgr")
    String qmgr;
    @ConfigProperty(name = "mq.appUser")
    String appUser;
    @ConfigProperty(name = "mq.appPassword")
    Optional<String> appPassword;
    @ConfigProperty(name = "mq.nameQueueRQ")
    String queueNameReq;
    @ConfigProperty(name = "mq.nameQueueRS")
    String queueNameRes;
    @ConfigProperty(name = "header.function")
    String appName;

    @Override
    public ResponseBroker calcularRetiroAnticipado(RequestCustom msj)  {

        if (msj.getNroDocumento() == null || msj.getTipoDocumento() == null) {

            String errorCode = "ERR0001";
            String msgError = "Es necesario un número de documento para poder realizar la consulta";
            String descError = "Ha ocurrido un error mientras se recibía la petición. " +
                    "Asegúrese que el número de Documento o tipo de Documento se envie en la petición";


            ResponseBroker responseBroker = consultarAS400.generateError(errorCode, msgError, descError);

            responseBroker.setHeader(msj.getHeader());

            return responseBroker;

        }

        //Se envía el REQUEST para Parsear al REQUEST del AS400
        Objeto xml = consultarAS400.ConvertToXml(msj);

        if (xml.getError() != null) {

            String errorCode = xml.getError().split("|")[1];
            String msgError = xml.getError().split("|")[2];
            String descError = "Ha ocurrido un error durante el proceso Marshall del Java," +
                    " favor verificar el codigo de error y su mensaje. Asegurese de que el parseo que se este realizando " +
                    "sea el correcto";


            ResponseBroker responseBroker = consultarAS400.generateError(errorCode, msgError, descError);

            responseBroker.setHeader(msj.getHeader());

            return responseBroker;

        }

        ConsumoMq mqParameter = new ConsumoMq.ConsumoMqBuilder(
                host,
                port,
                channel,
                qmgr,
                appUser,
                appPassword.orElse(""),
                queueNameReq).
                setApplicationName(appName).setCorrelId(xml.getCorrelID()).setRequiredAutentication(Boolean.FALSE).build();

        ConsumoMq mqParameterRS = new ConsumoMq.ConsumoMqBuilder(
                host,
                port,
                channel,
                qmgr,
                appUser,
                appPassword.orElse(""),
                queueNameRes).
                setApplicationName(appName).setCorrelId(BaseUtils.getCorrelIdHexa(xml.getCorrelID())).setRequiredAutentication(Boolean.FALSE).build();

        //Se envía y se recibe por MQ al AS400
        try {

            String responseAS400 = mqService.consumirIbmEnvioRecibirMq(mqParameter, mqParameterRS, xml.getXml(), new LogFile());

            //Se envia el RESPONSE del AS400 para Parsear al RESPONSE
            ResponseBroker responseBroker = consultarAS400.ConvertToJavaObject(responseAS400);
            responseBroker.setHeader(msj.getHeader());

            return responseBroker;

        } catch (Exception e) {

            String errorCode = "ERR0002";
            String msgError = "No se ha recibido respuesta del AS400";
            String descError = "Ha ocurrido un error durante el proceso get en la cola ESB.TM_INFVENCIMIENTOCTA.RS," +
                    " favor verificar el codigo de error y su mensaje. Asegurese de que el parseo que se este realizando " +
                    "sea el correcto";

            ResponseBroker responseBroker = consultarAS400.generateError(errorCode, msgError, descError);

            responseBroker.setHeader(msj.getHeader());
            return responseBroker;

        }
    }


    }

