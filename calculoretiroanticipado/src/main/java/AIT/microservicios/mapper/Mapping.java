package AIT.microservicios.mapper;

import AIT.core.base.pojos.microservicios.GenericResponse;
import AIT.core.base.pojos.microservicios.Respuesta;
import AIT.microservicios.model.requestAS400.Data;
import AIT.microservicios.model.responseAS400.Message;
import AIT.microservicios.model.share.Cust_Info;
import AIT.microservicios.model.share.Header;
import AIT.microservicios.pojo.request.RequestCustom;
import AIT.microservicios.pojo.response.*;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.text.SimpleDateFormat;
import java.util.Date;

@ApplicationScoped
public class Mapping {

    @ConfigProperty(name = "mq.channel")
    String channel;
    @ConfigProperty(name = "mq.qmgr")
    String qmgr;
    @ConfigProperty(name = "header.rmt_srvr")
    String rmt_svr;
    @ConfigProperty(name = "mq.nameQueueRS")
    String queueNameRes;
    @ConfigProperty(name = "header.function")
    String appName;
    public ResponseBroker Mapping_AS400_Broker(Message msj) {
        //MAPEO RESPONSE BROKER

        CalculoSancion calculosSancion = new CalculoSancion(
                msj.getData().getInformacion().getCalculosSancion().getFechaEfectiva(),
                msj.getData().getInformacion().getCalculosSancion().getSaldo(),
                msj.getData().getInformacion().getCalculosSancion().getInteresAdeudado(),
                msj.getData().getInformacion().getCalculosSancion().getInteresRetenido(),
                msj.getData().getInformacion().getCalculosSancion().getTasaInteres(),
                msj.getData().getInformacion().getCalculosSancion().getMontoSancion(),
                msj.getData().getInformacion().getCalculosSancion().getSancionNoCubiertaIntaCum(),
                msj.getData().getInformacion().getCalculosSancion().getTasaSancion(),
                msj.getData().getInformacion().getCalculosSancion().getDiasFaltantesVencimiento(),
                msj.getData().getInformacion().getCalculosSancion().getIntUltimaRenov(),
                msj.getData().getInformacion().getCalculosSancion().getTasaInteresRecalculada(),
                msj.getData().getInformacion().getCalculosSancion().getInteresConfiscado(),
                msj.getData().getInformacion().getCalculosSancion().getSancionCapital(),
                msj.getData().getInformacion().getCalculosSancion().getSancionRetiro(),
                msj.getData().getInformacion().getCalculosSancion().getImpuestoPagar(),
                msj.getData().getInformacion().getCalculosSancion().getNetoCliente()
        );

        ProductoResponse productoResponse = new ProductoResponse(
                msj.getData().getInformacion().getProducto().getNumero(),
                msj.getData().getInformacion().getProducto().getMoneda(),
                msj.getData().getInformacion().getProducto().getDesMoneda()
        );

        Cliente cliente = new Cliente(
                msj.getData().getInformacion().getCliente().getNombreAbreviado()
        );

        CalculoRetiroAnticipadoResponse calculoRetiroAnticipadoResponse = new CalculoRetiroAnticipadoResponse(productoResponse,cliente,calculosSancion);

        ResponseBroker responseBroker = new ResponseBroker();

        responseBroker.setCalculoRetiroAnticipadoResponse(calculoRetiroAnticipadoResponse);

        GenericResponse genericResponse = new GenericResponse();

        genericResponse.setIdMsg("OK");
        genericResponse.setDescMsg("La operación fue realizada exitósamente");

        Respuesta respuesta = new Respuesta();

        respuesta.setGenericResponse(genericResponse);

        responseBroker.setRespuesta(respuesta);
        return responseBroker;
    }

    public AIT.microservicios.model.requestAS400.Message Mapping_Broker_AS400(RequestCustom msj)  {


        //MAPEO DEL RQ DE BROKER AL DE AS400
        AIT.microservicios.model.requestAS400.Producto PRODUCTO = new AIT.microservicios.model.requestAS400.Producto(
                msj.getCalculoRetiroAnticipado().getProducto().getNumero(),
                msj.getCalculoRetiroAnticipado().getProducto().getFechaRetiro(),
                msj.getCalculoRetiroAnticipado().getProducto().getMontoRetiro());

        Data DATA = new Data(PRODUCTO);

        Cust_Info CUST_INFO = new Cust_Info(
                msj.getTipoDocumento(),
                msj.getNroDocumento(),
                "",
                "",
                msj.getHeader().getUsuario());

        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
        Date date = new Date();
        String correlId;
        if(msj.getNroDocumento().length() > 7)
            correlId = (formatter.format(date)+msj.getNroDocumento()).substring(0,24);
        else
            correlId = formatter.format(date)+msj.getNroDocumento();


        Header HEADER = new Header(channel,
                rmt_svr,
                correlId,
                appName,
                queueNameRes,
                qmgr);

        AIT.microservicios.model.requestAS400.Message MESSAGE = new AIT.microservicios.model.requestAS400.Message("RQ",HEADER,CUST_INFO,DATA);

        return MESSAGE;
    }


}
